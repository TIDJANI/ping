<?php
session_start();

$bdd = new PDO('mysql:host=localhost;dbname=bdd_7_18', 'root', '');

if(isset($_POST['formconnexion'])) {
	 $mailconnect = htmlspecialchars($_POST['mailconnect']);
	 $mdpconnect = sha1($_POST['mdpconnect']);
	 if(!empty($mailconnect) AND !empty($mdpconnect)) {
			$requser = $bdd->prepare("SELECT * FROM Utilisateur WHERE email = ? AND motdepasse = ?");
			$requser->execute(array($mailconnect, $mdpconnect));
			$userexist = $requser->rowCount();
			if($userexist == 1) {
				 $userinfo = $requser->fetch();
				 $_SESSION['ID'] = $userinfo['ID'];
				 $_SESSION['nom'] = $userinfo['nom'];
				 $_SESSION['prenom'] = $userinfo['prenom'];

				 $_SESSION['mail'] = $userinfo['mail'];
				 header("Location: Compte_Utilisateur.php?id=".$_SESSION['ID']);
			} else {
				 $erreur = "Mauvais mail ou mot de passe !";
			}
	 } else {
			$erreur = "Tous les champs doivent être complétés !";
	 }
}
?>
<!DOCTYPE html>
<html>
	 <head>
			<title>Connexion</title>
			<meta charset="utf-8">
			<link rel="stylesheet"  href="bootstrap.min.css">
				 <link rel="stylesheet" type="text/css" href="forme.css">
	 </head>
	 <body  >

			<header>
						<div class="header">
							 <img src="Logo_ESIGELEC.jpg" id="bannière" alt="bannière du site" height="50" width="250">
							 <div class="header-right">
									<a  href="index.html">Accueil</a>
									<a  class="active" href="connexion.php">Connexion</a>
									<a href="about">About</a>
							 </div>
						</div>   
			</header>
			<main>
				 <div class="center-div-connexion" align="center">
						
						
						<form class="form-signin" method="POST" action="">
							 <h1 class="h3 mb-3 font-weight-normal">Connectez-vous!</h1>
							 <label for="email">Adresse mail: </label>
							 <input class="form-control" type="email" name="mailconnect" placeholder="Mail" id="email" autofocus required="" />
							 <label for="motDePasse">Mot de Passe: </label>
							 <input class="form-control" type="password" name="mdpconnect" placeholder="Mot de passe" id="motDePasse" required/>
							 <a href="">Mot de passe oublié?</a>

							 <div class="checkbox mb-3">
									<label>
										 <input type="checkbox" value="remember-me"> Remember me
									</label>
							</div>
							 <button class="btn btn-lg btn-primary btn-block" type="submit" name="formconnexion">Se connecter!</button>
							 
						</form>
						<?php
						if(isset($erreur)) {
							 echo '<font color="red">'.$erreur."</font>";
						}
						?>
				 </div>
			</main>
	 </body>
</html>