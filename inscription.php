<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Inscription</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initiale-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<style type="text/css">
		body{
			padding: 0px;
			margin: 0px;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-light bg-light mb-4">
  		<span class="navbar-brand mb-0 h1"><a href="index.php">Projet PING 2019</a></span>
  		<a href="connexion.php" class="btn btn-outline-primary " >Connexion</a>
	</nav>

	<form method="POST" action="controller/inscriptionCtrl.php" class="w-75 m-auto">
	  <div class="form-group">
	    <label for="firstname">Prénom</label>
	    <input type="firstname" class="form-control" id="firstname" placeholder="" name="firstname">
	    <small class="form-text text-muted"></small>
	  </div>
	  <div class="form-group">
	    <label for="lastname">Nom</label>
	    <input type="lastname" class="form-control" id="lastname" placeholder="" name="lastname">
	    <small class="form-text text-muted"></small>
	  </div>
	  <div class="form-group">
	    <label for="email">Email</label>
	    <input type="email" class="form-control" id="email" placeholder="" name="email">
	    <small class="form-text text-muted"></small>
	  </div>
	  <div class="form-group">
	    <label for="password">Mot de Passe</label>
	    <input type="password" class="form-control" id="password" placeholder="" name="password">
	  </div>
	  <button type="submit" class="btn btn-primary">Envoyez</button>
	</form>
</body>
</html>