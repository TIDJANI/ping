<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Titre de page -->
    <title>Votes</title>

    <!-- Fichier css  -->
    <link rel="stylesheet" href="style.css">

    <!-- Css Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
  <!-- Corps de la page -->
  <div class="container-fluid">
  <!-- Barre de navigation -->
<nav class="navbar navbar-light bg-light">
            <!-- Logo -->
            <a class="navbar-brand" href="index.php">
                <img src="logo.png" width="" height="40" class="d-inline-block align-top" alt="">
            </a>
            
            <!-- Liens -->
            <span>
                <a href="Connexion.php"> Connexion </a>
                <a href="ajout.php"> AjoutPosters </a>
                <a href="Ping.php"> Votes </a>
                <a href="Classements.php"> Classements </a>
            </span>
        </nav>


    <h1 class="display-4">Listes des Pings</h1>
    
    <!-- Infos Votes -->
    <div class="container-fluid">
        <div class="row">

                <div class="col">
                    <!-- Carte -->
                    <div class="card" style="width: 15rem;">
                        <img src="./images/ping1.jpg" class="card-img-top" alt="...">
                                
                        <div class="card-body">
                            <h5 class="card-title">PING 1</h5>
                            <button type="button" data-toggle="modal" data-target="#ping1" class="btn btn-success">Infos</button>
                            <button type="button" class="btn btn-outline-primary">Voter</button>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="ping1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <!-- En-tête du modal -->
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalCenterTitle">PING 1</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <!-- Corps du modal -->
                                    <div class="modal-body">
                                        Détails des infos du Ping 1
                                    </div>

                                    <!-- Pied du modal -->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col">
                        <!-- Carte -->
                        <div class="card" style="width: 15rem;">
                            <img src="./images/ping2.jpg" class="card-img-top" alt="...">
                                    
                            <div class="card-body">
                                <h5 class="card-title">PING 2</h5>
                                <button type="button" data-toggle="modal" data-target="#ping2" class="btn btn-success">Infos</button>
                                <button type="button" class="btn btn-outline-primary">Voter</button>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="ping2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <!-- En-tête du modal -->
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">PING 2</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
    
                                        <!-- Corps du modal -->
                                        <div class="modal-body">
                                            Détails des infos du Ping 2
                                        </div>
    
                                        <!-- Pied du modal -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="col">
                            <!-- Carte -->
                            <div class="card" style="width: 15rem;">
                                <img src="./images/ping3.jpg" class="card-img-top" alt="...">
                                        
                                <div class="card-body">
                                    <h5 class="card-title">PING 3</h5>
                                    <button type="button" data-toggle="modal" data-target="#ping3" class="btn btn-success">Infos</button>
                                    <button type="button" class="btn btn-outline-primary">Voter</button>
                                </div>
                            </div>
                             <!-- Modal -->
                        <div class="modal fade" id="ping3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <!-- En-tête du modal -->
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">PING 3</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
    
                                        <!-- Corps du modal -->
                                        <div class="modal-body">
                                            Détails des infos du Ping 3
                                        </div>
    
                                        <!-- Pied du modal -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                                <!-- Carte -->
                                <div class="card" style="width: 15rem;">
                                    <img src="./images/ping4.jpg" class="card-img-top" alt="...">
                                            
                                    <div class="card-body">
                                        <h5 class="card-title">PING 4</h5>
                                        <button type="button" data-toggle="modal" data-target="#ping4" class="btn btn-success">Infos</button>
                                        <button type="button" class="btn btn-outline-primary">Voter</button>
                                    </div>
                                </div>
                                   <!-- Modal -->
                        <div class="modal fade" id="ping4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <!-- En-tête du modal -->
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">PING 4</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
    
                                        <!-- Corps du modal -->
                                        <div class="modal-body">
                                            Détails des infos du Ping 4
                                        </div>
    
                                        <!-- Pied du modal -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            
        </div>
  </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>